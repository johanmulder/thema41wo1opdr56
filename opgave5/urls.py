'''
Created on 14 sep. 2013

@author: johan
'''

from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^addSessionStuff', views.addToSession, name='addSessionStuff'),
)

