'''
Created on 14 sep. 2013

@author: johan
'''

from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse

def index(request):
    # Load the index.html template.
    template = loader.get_template('index.html')
    # Create a context with the session data in it.
    context = RequestContext(request, {'session': request.session})
    # Render the template with the context.
    return HttpResponse(template.render(context))

def addToSession(request):
    # Only do stuff when 'addToSession' is posted.
    if 'addToSession' in request.POST:
        # Init the sessionStuff array if not done yet.
        if not 'sessionStuff' in request.session:
            request.session['sessionStuff'] = []
        request.session['sessionStuff'].append(request.POST['addToSession'])
    # Redirect to prevent duplicate additions when a user reloads.
    return HttpResponseRedirect(reverse('opgave5:index'))